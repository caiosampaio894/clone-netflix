O projeto é uma reprodução da página da Netflix. Todas as informações foram usadas através da api do site The Movie DataBase: https://developers.themoviedb.org/3/discover/movie-discover. 

As tecnologias usadas foram react.js e css. 


No diretório do projeto, rode `yarn start`

Se for pressionado F5 para recarregar a página, o filme principal mudará.

### `yarn start`


### Deployment

O deploy foi feito pelo vercel: https://vercel.com/caiosampaio894/clone-netflix

