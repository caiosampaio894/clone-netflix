import React, {useEffect, useState}  from "react"
import Api from "./components/Api"
import MovieCart from "./components/MovieCart"
import './App.css'
import MainMovie from "./components/MainMovie"
import Header from "./components/Header"


export default () => {

  const [movieList, setMovieList] = useState([])
  const [movieData, setMovieData] = useState(null)

  useEffect(() => {
    const loadAll = async () => {
      let list = await Api.List()
      setMovieList(list)

      let or = list.filter(i => i.slug === 'originals')
      let randomPick = Math.floor(Math.random() * (or[0].items.results.length - 1))
      let pick = or[0].items.results[randomPick]
      let pickInfo = await Api.MovieInfo(pick.id, 'tv')
      setMovieData(pickInfo)

    }

    loadAll()
  }, [])

  return (
    <div className='page'>

      <Header/>

      {movieData &&
        <MainMovie item={movieData}/>
      }

      <section className='lists'>
        {movieList.map((item, key) => (
          <MovieCart key={key} title={item.title} items={item.items}/>
        ))}
      </section>
    </div>
  )
}