import React from "react";
import './styles.css'

export default () => {
    return (
        <header>
            <div className='header'>
                <a href=''>
                    <img src='https://upload.wikimedia.org/wikipedia/commons/0/08/Netflix_2015_logo.svg' />
                </a>
            </div>
        </header>
    )
}