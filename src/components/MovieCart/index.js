import React, { useState } from "react";
import './styles.css';
import { BsChevronDoubleRight, BsChevronDoubleLeft } from "react-icons/bs";


export default ({title, items}) => {
    return (
        <div className="movieCart">
            <h2>{title}</h2>
            <div className='arrowLeft'>
                <BsChevronDoubleLeft/>
            </div>
            <div className='arrowRight'>
                <BsChevronDoubleRight/>
            </div>
            <div className="movieCartList">
                <div className="movieCartSubList">
                    {items.results.length > 0 && items.results.map((item, key) => (
                        <div key={key} className="movieCartItem">
                            <img src={`https://image.tmdb.org/t/p/w300${item.poster_path}`} alt={item.original_title}/>
                        </div>
                    ))}
                </div>
            </div>
        </div>  
    )
}