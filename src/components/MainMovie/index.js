import './styles.css'

export default ({item}) => {

    let date = new Date(item.first_air_date)
    let genres = []
    for(let i in item.genres) {
        genres.push(item.genres[i].name);
    }
    
    return (
        <section className='main' style={{
            backgroundImage: `url(https://image.tmdb.org/t/p/original${item.backdrop_path})`
        }}>
            <div className='transparencyVertical'>
                <div className='transparencyHorizontal'>
                    <div className='MovieName'>{item.original_name}</div>
                    <div className='info'>
                        <div className='points'>{item.vote_average} pontos</div>
                        <div className='year'>{date.getFullYear}</div>
                        <div className='seasons'>{item.number_of_seasons} temporada{item.number_of_seasons !== 1 ? 's' : ''}</div>
                    </div>
                    <div className='description'>{item.overview}</div>
                    <div className='buttons'>
                        <a href='' className='watchButton'><span>▶</span> Assistir</a>
                        <a href='' className='listButton'>+ Minha Lista</a>
                    </div> 
                    <div className='genres'><strong>Gêneros: </strong>{genres.join(', ')}</div>
                </div>
            </div>
        </section>
    )
}